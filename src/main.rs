use std::path::Path;

use futures::StreamExt;
use structopt::StructOpt;
use tokio::fs::File;
use tokio::io::{stdin, BufReader};
use tokio::prelude::*;

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(short)]
    z: bool,
    #[structopt(short)]
    //#[structopt(short, default_value="$PWD")]
    base: Option<String>,
    file: Option<String>,
    /// Input is git Oid
    #[structopt(short)]
    o: bool,

    /// Operate in CACHE mode
    #[structopt(short, required_unless("u"), conflicts_with("u"))]
    c: bool,
    /// Operate in DUPE mode
    #[structopt(short, required_unless("c"), conflicts_with("c"))]
    u: bool,
}

async fn to_cache<P: AsRef<Path>>(line: P) {
    println!("done");
    if let Err(e) = tokio::fs::read(line.as_ref()).await {
        eprintln!("{}: {:?}", line.as_ref().display(), e);
    }
}

async fn to_upper<P: AsRef<Path>>(path: P) {
    let md = match tokio::fs::symlink_metadata(path.as_ref().to_owned()).await {
        Err(_) => return,
        Ok(md) => md,
    };
    if let Err(e) = tokio::fs::set_permissions(path.as_ref(), md.permissions()).await {
        eprintln!("utime fail: {} {:?}", path.as_ref().display(), e);
    }
}

#[paw::main]
#[tokio::main]
async fn main(opt: Opt) -> Result<(), Box<dyn std::error::Error>> {
    let hard = rlimit::Resource::NOFILE.get()?.1;
    rlimit::Resource::NOFILE.set(hard, hard)?;
    let source: Box<dyn AsyncRead + Unpin> = match opt.file.as_ref().map(String::as_ref) {
        Some("-") | None => Box::new(stdin()),
        Some(path) => Box::new(File::open(path).await?),
    };
    let mut stm = BufReader::new(source).lines();
    let mut bg_jobs = futures::stream::FuturesUnordered::new();
    while let Some(mut line) = stm.next_line().await? {
        if opt.o {
            line = format!(".git/objects/{}/{}", &line[..2], &line[2..]);
        }
        if opt.c {
            bg_jobs.push(tokio::spawn(to_cache(line)));
        } else if opt.u {
            bg_jobs.push(tokio::spawn(to_upper(line)));
        }
    }
    while let Some(_) = bg_jobs.next().await {}
    //bg_jobs.for_each(|_| futures::future::ready(())).await;
    //bg_jobs
    //    .for_each_concurrent(0, |_| futures::future::ready(()))
    //    .await;
    Ok(())
}
