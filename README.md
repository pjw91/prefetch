# OverlayFS Prefetcher

When lowerdir is on a slow medium with caching (eg. rclone), this program reads filenames from stdin and:
1. reads the file: which triggers the rclone to cache remote file
2. chmod the file as-is: which triggers OverlayFS to copy the file from lowerdir to upperdir
All operations are run asynchronously.

Usage:
1. `-c`: cache mode
2. `-u`: duplicate mode
3. `-o`: reads git object id from stdin instead of filename
